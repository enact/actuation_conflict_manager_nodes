let mqtt = require('mqtt');

module.exports = function (RED) {
	function acm_sync(config) {
		RED.nodes.createNode(this, config);
		var node = this;

        node.acmtags = JSON.parse(config.payload);
		node.acmsaved = generateSaved(node.acmtags);
		node.acmflownames = generateFlowNames(node.acmtags);
		node.acmtimeout = null;
		node.delay = config.delay;
		node.conflictID = config.conflictID;
		node.brokerAddress = config.brokerAddress;
		node.brokerOK = false;
		node.conditionToTrigger = config.conditionToTrigger || "" //"input[0][0]!=null&&input[0][1]!=null&&input[0][2]!=null"

		if (node.brokerAddress !== "unset") {
			node.client = mqtt.connect(node.brokerAddress);
			node.status({ fill: "red", shape: "ring", text: "MQTT broker address invalid" });
		}

		node.client.on('connect', function () {
			if (node.conditionToTrigger == "")
				node.acmtimeout = setTimeout(node.sendAfterTimeout, node.delay);
			node.client.subscribe("acm/" + node.conflictID, function (err) {
				if (err) {
					node.status({ fill: "red", shape: "ring", text: "MQTT error" });
					console.log("error connecting to MQTT");
					console.log(err);
				} else {
					node.brokerOK = true
					node.status({ fill: "green", shape: "dot", text: "MQTT OK" });
				}
			});
		});

		node.sendAfterTimeout = function () {
			node.acmtimeout = 0;
			var output = {};
			output.payload = [];
			output.tagNames = [];
			for (let afn in node.acmflownames) {
				output.tagNames.push(node.acmflownames[afn]);
            }
            for (var savedItem in node.acmsaved) {
				output.payload.push(node.acmsaved[savedItem]);
            }
            node.acmsaved = generateSaved(node.acmtags);
			node.send(output);
			if (node.conditionToTrigger == "")
				node.acmtimeout = setTimeout(node.sendAfterTimeout, node.delay);
		};

		node.client.on('message', function (topic, message) {
			let msg = JSON.parse(message.toString());
			if (!node.acmsaved[msg.acmTag]) {
				console.log("acm sync " + node.id + " error: received msg with tag " + msg.acmTag + " that cant be handled, skipping");
				return;
            }
			node.acmsaved[msg.acmTag].push(msg)/*.payload*/;
			node.acmflownames[msg.acmTag] = msg.acmFlowName;
				
			//condition + delay
			if(ConditionIsVerified(node.conditionToTrigger,node.acmsaved) && node.delay!==""){
				if (!node.acmtimeout) {
					node.acmtimeout = setTimeout(node.sendAfterTimeout, node.delay);
				}
			}
			//only condition
			else if(ConditionIsVerified(node.conditionToTrigger,node.acmsaved)){	
				node.sendAfterTimeout()
			}
			
			/*
			if(ConditionIsVerified(node.conditionToTrigger,node.acmsaved)){
				console.log("condition verified in ACM sync")
				node.sendAfterTimeout()
				
				if (!node.acmtimeout && node.delay) {
					node.acmtimeout = setTimeout(node.sendAfterTimeout, node.delay);
				}else{
				}
			}
			else if (!node.acmtimeout) {
				node.acmtimeout = setTimeout(node.sendAfterTimeout, node.delay);
			}
			*/
		});

		node.on('input', function (msg) {
			if (node.brokerOK) {
				let payload = JSON.parse(msg.payload);
				msg.payload = payload.acmPayload;
				msg.acmTag = payload.acmTag;
				msg.acmFlowName = payload.acmFlowName;
				node.client.publish("acm/" + node.conflictID, JSON.stringify(msg));
			}
		});

		node.on('close', function (done) {
			clearTimeout(node.acmtimeout);
			node.client.end();

			done();
		});
	}
	RED.nodes.registerType("acm-sync", acm_sync);
};

function generateSaved(tags) {
	let newSaved = {};
	for (var tagidx in tags) {
		newSaved[tags[tagidx]] = [];
	}
	return newSaved;
}

function generateFlowNames(tags) {
	let newSaved = {};
	for (var tagidx in tags) {
		newSaved[tags[tagidx]] = "unknown";
	}
	return newSaved;
}

function ConditionIsVerified(cond, saved){
	var input = []
	
	if(cond==""){
		return false
	}
	for (var savedItem in saved) {
		var sub_input = []
		for (let j = 0; j < saved[savedItem].length; j++) {
			sub_input.push(saved[savedItem][j].payload);		
		}
		input.push(sub_input)
	}
	//return eval("true")
	return eval(cond)
}
