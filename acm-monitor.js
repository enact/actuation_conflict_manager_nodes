let mqtt = require('mqtt');

module.exports = function (RED) {
    function acm_monitor(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        node.name = config.name;
        node.montype = config.montype;
        node.action = config.action;
        node.source = config.source;
        node.destination = config.destination;
        node.mqttAddress = config.mqttAddress;
        node.brokerOK = false;

        node.connectToMQTT = function () {
            // connect to MQTT
            if (node.brokerAddress !== "unset") {
                node.client = mqtt.connect(node.brokerAddress);

                node.client.on('connect', function () {
                    node.brokerOK = true;
                });

                node.client.on("close", function () {
                    node.brokerOK = false;
                    node.client.end();
                    // try to reconnect after 1s?
                    setTimeout(node.connectToMQTT, 1000);
                });
            }
        };
        node.connectToMQTT();

        // main function
        node.on('input', function (msg) {
            // only log if strat is log
            if (node.action === "log") {
                if (node.brokerOK) {
                    node.client.publish(
                        "acm-monitor/" + this.source + "/" + this.destination,
                        JSON.stringify({
                            id: node.id,
                            timestamp: Date.now(),
                            source: node.source,
                            destination: node.destination,
                            montype: node.montype,
                            data: msg.payload
                        })
                    );
                } else {
                    console.log("acm monitor attempted to log but broker nok");
                }
            }

            // passthrough all the time
            node.send(msg);
        });

        // cleenup
        node.on('close', function (done) {
            node.client.end();
            done();
        });
    }
    RED.nodes.registerType("acm-monitor", acm_monitor);
}