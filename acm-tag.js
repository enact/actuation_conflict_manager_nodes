module.exports = function (RED) {
    function acm_tag(config) {
		RED.nodes.createNode(this, config);
		let tag = config.tag;
        var node = this;

        node.on('input', function (msg) {
            msg.acmTag = tag;
            msg.payload = JSON.stringify({ acmComposite: true, acmTag: tag, acmPayload: msg.payload, acmFlowName: config._flow.flow.label });
            node.send(msg);
        });
    }
	RED.nodes.registerType("acm-tag", acm_tag);
}