var xstate = require('xstate')
var Machine = xstate.Machine
var interpret = xstate.interpret
var send = xstate.send
//var toJson = require("./fromSCXMLToJson")
const xml2js = require('xml2js');
const fs = require('fs');
const parser = new xml2js.Parser({ $key: "$" });

class FSM {

    constructor() {
        this.current_state = "";
        this.fsm = {}
        this.predicates = {}
        this.actions = {}
    }



    input(payloads) {
        console.log("payloads")
        console.log(payloads)
        console.log("this.current_state")
        console.log(this.current_state)

        var triggeredpredicates = []
        for (const p in this.predicates) {
            if(this.conditionVerified(this.predicates[p],payloads)){
                console.log("condition is verified")
                triggeredpredicates.push(p)
            }
        }
        console.log("triggeredpredicates")
        console.log(triggeredpredicates)
        let triggeredactions = [];
        for (var i = 0; i < triggeredpredicates.length; i++) {
            if(triggeredpredicates[i]!=null){
                const nextState = this.fsm.transition(this.current_state,triggeredpredicates[i]);
    			this.current_state = nextState.value
    			if(nextState.actions[0]){
                    for (var j = 0; j < nextState.actions.length; j++) {
                        triggeredactions.push(nextState.actions[j].event.type)
                    }
    			}
        	}
        }
        console.log("new state : "+this.current_state)
        console.log("actions triggered : "+triggeredactions)
        
        let ret = []
        for (let i = 0; i < triggeredactions.length; i++) {
            var outputs = this.actions[triggeredactions[i]]
            for (var o in outputs){
                ret[o] = this.evalOutput(outputs[o],payloads)
            }
        }
        payloads = ret
    	return payloads
    }


    buildFSM(configuration){

        this.predicates = configuration.predicates
        console.log("here")
        this.actions = configuration.actions
    	var jsonfsm = this.getJSONMachine(configuration.scxml)	
    	this.fsm = Machine(jsonfsm)
        this.current_state = jsonfsm.initial
        
    }


    getJSONMachine(scxml){
        console.log("starting scxml transformation")
        var scxmlToJson =  this.parseSync(scxml);
        var states = {}
        var root = scxmlToJson.scxml;
        var fsm = root.state[0].state
        var inital_state = root.state[0].$.initial
        for (var i = 0; i < fsm.length; i++) {
            var stateName = fsm[i].$.id;
            var transitions = {}    
            if(fsm[i].transition){
                for (var j = 0; j < fsm[i].transition.length; j++) {
                    var target = {"target":fsm[i].transition[j].$.target}
                    
                    if(fsm[i].transition[j].send){
                        target.actions = []
                        for (var k = 0; k < fsm[i].transition[j].send.length; k++) {
                            target.actions.push(send(fsm[i].transition[j].send[k].$.event))
                        }
                    }
                    transitions[fsm[i].transition[j].$.event] = target
                }

            }
            states[stateName] = {"on":transitions}
        }
        var jsonfsm={"id":"ACM","initial":inital_state,"states":states}
        //console.log(JSON.stringify(jsonthis.fsm))
        return jsonfsm
    }


    parseSync (xml) {

        var error = null;
        var json = null;
        xml2js.parseString(xml, function (innerError, innerJson) {

            error = innerError;
            json = innerJson;
        });

        if (error) {

            throw error;
        }

        if (!error && !json) {

            throw new Error('The callback was suddenly async or something.');
        }

        return json;
    }


    conditionVerified(condition,input){
        return eval(condition)

    }


    evalOutput(output,input){
        if(Number.isInteger(output))
            return output
        if(output.includes("input"))
            return eval(output)
        //in case the topic is used for the conflict management
        if(output.includes("_"))
            return output.split("_")[1]
        return output

    }
    

}


module.exports = FSM;