# Actuation Conflict Manager Node-RED Nodes
These nodes should be installed in Node-RED in order for actuation management components to function.

## Installing in Node-RED
### Local instance
  - Navigate to the global Node-RED module folder
    - Windows: `cd %userprofile%\.node-red`
    - Linux/Mac: `cd ~/.node-red`
  - Install the module from git
    - `npm install https://gitlab.com/enact/actuation_conflict_manager_nodes`

### Genesis
   - Add the git address to the packages parameter array